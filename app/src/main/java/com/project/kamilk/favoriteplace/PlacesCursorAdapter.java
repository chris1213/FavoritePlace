package com.project.kamilk.favoriteplace;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.project.kamilk.favoriteplace.abstract_helper.UIMethodsManager;
import com.project.kamilk.favoriteplace.constans.Contracts.DatabaseConstants;
import com.project.kamilk.favoriteplace.abstract_helper.LocationMethodsManager;

class PlacesCursorAdapter extends CursorAdapter {


    PlacesCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.adapter_places, viewGroup, false);
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {


        TextView titleAdapter = view.findViewById(R.id.title_adapter_tv);
        TextView noteAdapter = view.findViewById(R.id.note_adapter_tv);
        TextView addressAdapter = view.findViewById(R.id.address_adapter_tv);
        TextView longitudeAdapter = view.findViewById(R.id.longitude_adapter_tv);
        TextView latitudeAdapter = view.findViewById(R.id.latitude_adapter_tv);

        int title_column = cursor.getColumnIndex(DatabaseConstants.COLUMN_TITLE);
        int note_column = cursor.getColumnIndex(DatabaseConstants.COLUMN_NOTE);
        int address_column = cursor.getColumnIndex(DatabaseConstants.COLUMN_ADDRESS);
        int longitude_column = cursor.getColumnIndex(DatabaseConstants.COLUMN_LONGITUDE);
        int latitude_column = cursor.getColumnIndex(DatabaseConstants.COLUMN_LATITUDE);

        final String title = cursor.getString(title_column);
        String note = cursor.getString(note_column);
        String address = cursor.getString(address_column);
        final double longitude = cursor.getDouble(longitude_column);
        final double latitude = cursor.getDouble(latitude_column);

        titleAdapter.setText(title);
        noteAdapter.setText(note);
        addressAdapter.setText(address);
        longitudeAdapter.setText(String.valueOf(longitude));
        latitudeAdapter.setText(String.valueOf(latitude));


        ImageView editPlace_iv = view.findViewById(R.id.edit_place_imageview);
        editPlace_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "edit button", Toast.LENGTH_SHORT).show();
            }
        });

        ImageView goToLocation_iv = view.findViewById(R.id.go_to_location_imageview);
        goToLocation_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationMethodsManager.goToLocation(context, title, latitude, longitude);
            }
        });

        final int position = cursor.getPosition();

        ImageView sendUp_btn = view.findViewById(R.id.send_up_btn);
        sendUp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIMethodsManager.LIST_ELEMENT_UP = true;
                UIMethodsManager.MoveTheListItem(context, cursor, position);
            }
        });

        ImageView sendDown_btn = view.findViewById(R.id.send_down_btn);
        sendDown_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIMethodsManager.LIST_ELEMENT_UP = false;
                UIMethodsManager.MoveTheListItem(context, cursor, position);
            }
        });
    }

}
