package com.project.kamilk.favoriteplace.abstract_helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;


public abstract class LocationMethodsManager {

    private static LocationRequest mLocationRequest;

    public static LocationRequest createLocationRequest(LocationRequest mLocationRequest) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setInterval(6 * 1000);
//        mLocationRequest.setFastestInterval(5 * 1000);
        return mLocationRequest;
    }

    public static LocationSettingsRequest createLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(createLocationRequest(mLocationRequest));
        LocationSettingsRequest mLocationSettingsRequest = builder.build();
        return mLocationSettingsRequest;
    }

    public static void goToLocation(Context context, String title, double latitude, double longitude) {
        String uriBegin = "geo:" + latitude + "," + longitude;
        String query = latitude + "," + longitude + "(" + title + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        context.startActivity(intent);
    }

}




















