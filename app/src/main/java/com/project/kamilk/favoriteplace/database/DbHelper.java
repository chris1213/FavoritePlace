package com.project.kamilk.favoriteplace.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.project.kamilk.favoriteplace.constans.Contracts.DatabaseConstants;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "db_ver_1b";
    private static final int DB_VERSION = 1;

    private static final String SQL_UPGRADE_DB =
            "DROP TABLE IF EXISTS " + DatabaseConstants.TABLE_NAME;
    private static final String SQL_CREATE_DB =
            "CREATE TABLE " + DatabaseConstants.TABLE_NAME +
                    " (" + DatabaseConstants._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    DatabaseConstants.COLUMN_TITLE + " TEXT, " +
                    DatabaseConstants.COLUMN_NOTE + " TEXT, " +
                    DatabaseConstants.COLUMN_ADDRESS + " TEXT, " +
                    DatabaseConstants.COLUMN_LONGITUDE + " REAL, " +
                    DatabaseConstants.COLUMN_LATITUDE + " REAL)";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SQL_CREATE_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1) {
        database.execSQL(SQL_UPGRADE_DB);
        onCreate(database);
    }
}
