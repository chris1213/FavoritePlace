package com.project.kamilk.favoriteplace.constans;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.provider.BaseColumns;

import com.project.kamilk.favoriteplace.PlacesListActivity;


public class Contracts {

    public static class IntentServiceConstants{

        public static final int SUCCESS_RESULT = 0;
        public static final int FAILURE_RESULT = 1;
        public static final String PACKAGE_NAME =
                "com.project.kamilk.favoriteplace.locationaddress";
        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
        public static final String RESULT_DATA_KEY = PACKAGE_NAME +
                ".RESULT_DATA_KEY";
        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
                ".LOCATION_DATA_EXTRA";
    }

    public static class MenuItemConstants {

        public static boolean menuListItemVisible = true;
        public static boolean menuDetailItemVisible = true;
        public static boolean menuEditItemVisible = true;

        public static void changeIcons_Edit(Activity activity) {
            menuEditItemVisible = true;
            menuDetailItemVisible = false;
            menuListItemVisible = false;
            activity.invalidateOptionsMenu();
        }

        public static void changeIcons_Detail(Activity activity) {
            menuEditItemVisible = false;
            menuDetailItemVisible = true;
            menuListItemVisible = false;
            activity.invalidateOptionsMenu();
        }

        public static void changeIcons_List(Activity activity) {
            menuEditItemVisible = false;
            menuDetailItemVisible = false;
            menuListItemVisible = true;
            activity.invalidateOptionsMenu();
            activity.navigateUpTo(new Intent(activity, PlacesListActivity.class));
        }
    }

    public static class ProviderConstants {

        public static final String PLACE_AUTORITHY = "com.project.kamilk.favoriteplace.provider"; //symboliczna nazwa dostawcy treści
        public static final String PLACE_PATH = "Places"; //nazwa identyfikująca tabelę
        public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + PLACE_AUTORITHY);
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PLACE_PATH);

        public static final int PLACES = 100;
        public static final int PLACE_ID = 101;

        public static final String LIST_TYPE_ONE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + PLACE_AUTORITHY + "/" + PLACE_PATH;
        public static final String LIST_TYPE_MANY = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + PLACE_AUTORITHY + "/" + PLACE_PATH;

        public static final int LOADER_ID = 1;
    }

    public static class DatabaseConstants implements BaseColumns {

        public static final String TABLE_NAME = "database_table";
        public static final String COLUMN_TITLE = "column_title";
        public static final String COLUMN_NOTE = "column_note";
        public static final String COLUMN_ADDRESS = "column_address";
        public static final String COLUMN_LONGITUDE = "column_longitude";
        public static final String COLUMN_LATITUDE = "column_latitude";
    }

}
