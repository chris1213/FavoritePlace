package com.project.kamilk.favoriteplace.abstract_helper;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Toast;

import com.project.kamilk.favoriteplace.PlacesAddEditActivity;

public class OtherMethodsManager {

    public static void makeToast(Context context, String tekst){
        Toast.makeText(context, tekst, Toast.LENGTH_SHORT).show();
    }

    public static void showSnackbar(Activity activity, final int mainTextStringId, final int actionStringId, View.OnClickListener listener){
        Snackbar.make(
                activity.findViewById(android.R.id.content),
                activity.getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(activity.getString(actionStringId), listener)
                .show();
    }

    public static boolean checkPermission(Context context, String permission ) {
        int permissionState = ActivityCompat.checkSelfPermission
                (context, permission);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public static void checkPermissionsListedInManifest(Activity activity, String[] permission, int requestCode){
        ActivityCompat.requestPermissions(
                activity,
                permission,
                requestCode
        );
    }
}
